## from os import urandom
from Crypto.Cipher import AES

# For Generating cipher text
secret_key = "abcdefghABCDEFGH" 
iv = "1234567890123456"
objECB = AES.new(secret_key, AES.MODE_ECB, iv)
#objCBC = AES.new(secret_key, AES.MODE_CBC, iv)
#objCFB = AES.new(secret_key, AES.MODE_CFB, iv)
#objOFB = AES.new(secret_key, AES.MODE_OFB, iv)
#objCTR = AES.new(secret_key, AES.MODE_CTR, iv)
#objOPENPGP = AES.new(secret_key, AES.MODE_OPENPGP, iv)

# Encrypt the message
message = '3RsZT#vzP40%!@#$'
print('Original message is: ', message)
encrypted_text1 = objECB.encrypt(message)
#encrypted_text2 = objCBC.encrypt(message)
#encrypted_text3 = objCFB.encrypt(message)
#encrypted_text4 = objOFB.encrypt(message)
#encrypted_text5 = objCTR.encrypt(message)
#encrypted_text6 = objOPENPGP.encrypt(message)

print('The encrypted text ECB', encrypted_text1)
#print('The encrypted text CBC', encrypted_text2)
#print('The encrypted text CFB', encrypted_text3)
#print('The encrypted text OFB', encrypted_text4)
#print('The encrypted text CTR', encrypted_text5)
#print('The encrypted text OPENPGP', encrypted_text6)

