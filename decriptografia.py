from base64 import b64encode, b64decode
import base64
import hashlib
from Cryptodome.Cipher import AES
import os
from Cryptodome.Random import get_random_bytes

def decrypt(enc_dict, password):
    # decode the dictionary entries from base64
    salt = b64decode(enc_dict['salt'])
    cipher_text = b64decode(enc_dict['cipher_text'])
    nonce = b64decode(enc_dict['nonce'])
    tag = b64decode(enc_dict['tag'])
    
    # generate the private key from the password and salt
    private_key = hashlib.scrypt(
        password.encode(), salt=salt, n=2**14, r=8, p=1, dklen=32)

    # create the cipher config
    cipher = AES.new(private_key, AES.MODE_GCM, nonce=nonce)

    # decrypt the cipher text
    decrypted = cipher.decrypt_and_verify(cipher_text, tag)

    return decrypted


semente = "3abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
chave = bytes.hex({'cipher_text': 'q8yo1L1yF1lj6jH/zFPiQA==', 'salt': '9LeENTLZNK5qwPtAqF1ZQA==', 'nonce': '+cmoIl+ek+qX8dbO5E9+aA==', 'tag': 'nWkaLRYxiGAiZr9Q0M8RmQ=='})


# Let us decrypt using our original password
decrypted = decrypt(chave, semente)

print("hash decriptografado: ", bytes.decode(decrypted))

